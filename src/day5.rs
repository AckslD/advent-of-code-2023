use std::cmp;

use nom::sequence::{tuple, pair, preceded};
use nom::character::complete::alpha1;
use nom::bytes::complete::tag;
use nom::combinator::map;
use nom::multi::many1;

use crate::utils::read_text;
use crate::parse::{digit, ParseRes};

pub fn p1() {
    let text = read_text("day5.txt");
    let almanac = parse_almanac(&text).unwrap().1;
    let (seeds, maps) = almanac;
    let smallest_location = seeds.iter()
        .map(|s| get_location(&maps, *s))
        .min().unwrap();
    println!("{}", smallest_location);
}

pub fn p2() {
    let text = read_text("day5.txt");
    let almanac = parse_almanac(&text).unwrap().1;
    let (seeds, maps) = almanac;
    let mut seed_ranges: Vec<Range> =  Vec::new();
    for n in 0..(seeds.len() / 2) {
        let start = seeds[2 * n];
        let range = seeds[2 * n + 1];
        seed_ranges.push(Range{start, end: start + range});
    }
    get_location_ranges(&maps, &seed_ranges[0]);
    let smallest_location = seed_ranges.iter()
        .map(|r| get_location_ranges(&maps, r).iter().map(|nr| nr.start).min().unwrap())
        .min().unwrap();
    println!("{}", smallest_location);
}

fn get_location(maps: &Maps, seed: u64) -> u64 {
    let mut number = seed;
    for map in maps.iter() {
        // NOTE we ignore the title and assume it's correct
        number = apply_map(&map.1, number);
    }
    number
}

fn apply_map(map_body: &Body, number: u64) -> u64 {
    for map_entry in map_body {
        if let Some(n) = apply_map_entry(map_entry, number) {
            return n;
        }
    }
    number
}

fn apply_map_entry(map_entry: &(u64, u64, u64), number: u64) -> Option<u64> {
    let (dest_start, source_start, range) = map_entry;
    if number >= *source_start && number < source_start + range {
        Some(number - source_start + dest_start)
    } else {
        None
    }
}

#[derive(Debug, Clone)]
struct Range {
    start: u64,
    end: u64,
}

impl Range {
    fn cut(&self, range: &Self) -> Cut {
        Cut{
            // left
            left: Self::new_if_valid(
                self.start,
                cmp::min(self.end, range.start),
            ),
            // middle
            middle: Self::new_if_valid(
                cmp::max(self.start, range.start),
                cmp::min(self.end, range.end),
            ),
            // right
            right: Self::new_if_valid(
                cmp::max(self.start, range.end),
                self.end,
            ),
        }
    }

    fn new_if_valid(start: u64, end: u64) -> Option<Self> {
        if start < end {
            Some(Self{start, end})
        } else {
            None
        }
    }
}

#[derive(Debug)]
struct Cut {
    left: Option<Range>,
    middle: Option<Range>,
    right: Option<Range>,
}

fn get_location_ranges(maps: &Maps, range: &Range) -> Vec<Range> {
    let mut ranges: Vec<Range> = vec![range.clone()];
    for map in maps.iter() {
        // NOTE we ignore the title and assume it's correct
        let mut new_ranges = Vec::new();
        for range in ranges.iter() {
            for new_range in update_ranges_from_map(&map.1, range) {
                new_ranges.push(new_range);
            }
        }
        ranges = new_ranges;
    }
    ranges
}

fn update_ranges_from_map(map_body: &Body, range: &Range) -> Vec<Range> {
    let mut old_ranges: Vec<Range> = vec![range.clone()];
    let mut new_ranges: Vec<Range> = Vec::new();
    for map_entry in map_body {
        update_ranges_from_map_entry(map_entry, &mut old_ranges, &mut new_ranges);
    }
    old_ranges.into_iter().chain(new_ranges.into_iter()).collect()
}

fn update_ranges_from_map_entry(
    map_entry: &(u64, u64, u64),
    old_ranges: &mut Vec<Range>,
    new_ranges: &mut Vec<Range>,
) {
    let (dest_start, source_start, range) = map_entry;
    let source_range = Range{start: *source_start, end: source_start + range};
    let mut tmp_old_ranges: Vec<Range> = Vec::new();
    for old_range in old_ranges.iter() {
        let cut = old_range.cut(&source_range);
        if let Some(r) = cut.left {
            tmp_old_ranges.push(r);
        }
        if let Some(r) = cut.middle {
            new_ranges.push(Range{start: r.start + dest_start - source_start, end: r.end + dest_start - source_start});
        }
        if let Some(r) = cut.right {
            tmp_old_ranges.push(r);
        }
    }
    old_ranges.clear();
    old_ranges.extend(tmp_old_ranges.into_iter());
}

type Almanac<'a> = (Seeds, Maps<'a>);

fn parse_almanac(s: &str) -> ParseRes<Almanac> {
    pair(
        seeds,
        maps,
    )(s)
}

type Seeds = Vec<u64>;

fn seeds(s: &str) -> ParseRes<Seeds> {
    map(pair(tag("seeds:"), numbers), |(_, n)| n)(s)
}

type Maps<'a> = Vec<Map<'a>>;

fn maps(s: &str) -> ParseRes<Maps> {
    many1(preceded(tag("\n\n"), one_map))(s)
}

type Map<'a> = (Title<'a>, Body);

fn one_map(s: &str) -> ParseRes<Map> {
    pair(
        map_title,
        map_body,
    )(s)
}

type Title<'a> = (&'a str, &'a str);

fn map_title(s: &str) -> ParseRes<Title> {
    map(tuple((
        alpha1,
        tag("-to-"),
        alpha1,
        tag(" map:"),
    )), |(s, _, d, _)| (s, d))(s)
}

type Body = Vec<(u64, u64, u64)>;

fn map_body(s: &str) -> ParseRes<Body> {
    many1(map_line)(s)
}

fn map_line(s: &str) -> ParseRes<(u64, u64, u64)> {
    preceded(tag("\n"), tuple((digit, digit, digit)))(s)
}

fn numbers(s: &str) -> ParseRes<Vec<u64>> {
    many1(digit)(s)
}
