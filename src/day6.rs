use nom::bytes::complete::tag;
use nom::sequence::preceded;

use crate::utils::read_lines;
use crate::parse::numbers;

pub fn p1() {
    let (times, distances) = get_races("day6.txt");
    let product: u64 = times.iter().zip(distances.iter())
        .map(|(time, distance)| num_ways(*time, *distance))
        .product();
    println!("{}", product);
}

pub fn p2() {
    let lines = read_lines("day6.txt");
    let time: u64 = lines[0].replace("Time:", "").replace(" ", "").parse().unwrap();
    let distance: u64 = lines[1].replace("Distance:", "").replace(" ", "").parse().unwrap();
    println!("{}", num_ways(time, distance));
}

fn num_ways(time: u64, distance: u64) -> u64 {
    let (x0, x1) = solve_quadratic(-(time as f64), distance as f64).unwrap();
    // println!("{}, {}", x0, x1);
    // println!("{}, {}, {}", floor_exclusive(x1), ceil_exclusive(x0), floor_exclusive(x1) - ceil_exclusive(x0) + 1);
    floor_exclusive(x1) - ceil_exclusive(x0) + 1
}

fn solve_quadratic(p: f64, q: f64) -> Option<(f64, f64)> {
    let f = (p / 2.).powf(2.) - q;
    if f < 0. {
        return None;
    }
    let x = ((p / 2.).powf(2.) - q).sqrt();
    Some((-p / 2. - x, -p / 2. + x))
}

fn floor_exclusive(x: f64) -> u64 {
    let y = x.floor();
    if y == x {
        y as u64 - 1
    } else {
        y as u64
    }
}

fn ceil_exclusive(x: f64) -> u64 {
    let y = x.ceil();
    if y == x {
        y as u64 + 1
    } else {
        y as u64
    }
}

fn get_races(file: &str) -> (Vec<u64>, Vec<u64>) {
    let lines = read_lines(file);
    (parse_times(&lines[0]), parse_distances(&lines[1]))
}

fn parse_times(s: &str) -> Vec<u64> {
    preceded(tag("Time:"), numbers)(s).unwrap().1
}

fn parse_distances(s: &str) -> Vec<u64> {
    preceded(tag("Distance:"), numbers)(s).unwrap().1
}
