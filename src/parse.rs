use std::str::FromStr;
use std::fmt::Debug;

use nom::sequence::delimited;
use nom::character::complete::{digit1, space0};
use nom::combinator::map;
use nom::multi::many1;
use nom::IResult;

pub type ParseRes<'a, T> = IResult<&'a str, T>;

pub fn digit<T>(s: &str) -> ParseRes<T>
where
    T: FromStr, <T as FromStr>::Err : Debug
{
    map(delimited(space0, digit1, space0), |s: &str| s.parse().unwrap())(s)
}

pub fn numbers<T>(s: &str) -> ParseRes<Vec<T>>
where
    T: FromStr, <T as FromStr>::Err : Debug
{
    many1(digit)(s)
}
