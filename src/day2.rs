use std::collections::HashMap;

use nom::bytes::complete::tag;
use nom::sequence::{pair, tuple, delimited};
use nom::character::complete::{digit1, multispace0};
use nom::combinator::map;
use nom::multi::separated_list0;
use nom::branch::alt;
use nom::IResult;

use crate::utils::read_lines;

pub fn p1() {
    let lines = read_lines("day2.txt");
    let games = get_games(lines);
    let total: u32 = games.iter()
        .filter(|game| is_valid(game))
        .map(|game| game.num)
        .sum();
    println!("{}", total);
}

pub fn p2() {
    let lines = read_lines("day2.txt");
    let games = get_games(lines);
    let total: u32 = games.iter()
        .map(|game| power(game))
        .sum();
    println!("{}", total);
}

fn is_valid(game: &Game) -> bool {
    game.sets.iter().all(
        |cubes| cubes.red <= 12 && cubes.green <= 13 && cubes.blue <= 14
    )
}

fn power(game: &Game) -> u32 {
    let mut counts_per_color: HashMap<Color, Vec<u32>> = HashMap::new();
    for cubes in game.sets.iter() {
        for (color, count) in cubes.get_counts() {
            counts_per_color.entry(color).or_insert_with(|| Vec::new()).push(count);
        }
    }
    counts_per_color.values()
        .map(|counts| counts.iter().max().unwrap_or(&0))
        .product()
}

fn get_games(lines: Vec<String>) -> Vec<Game> {
    let mut games = Vec::new();
    for line in lines {
        games.push(parse_line(&line).unwrap().1);
    }
    games
}

type ParseRes<'a, T> = IResult<&'a str, T>;

fn digit(s: &str) -> ParseRes<u32> {
    map(delimited(multispace0, digit1, multispace0), |s: &str| s.parse().unwrap())(s)
}

#[derive(Debug, PartialEq, Eq, Hash)]
enum Color {
    Blue,
    Red,
    Green,
}

fn color(s: &str) -> ParseRes<Color> {
    alt((
        map(tag("blue"), |_| Color::Blue),
        map(tag("red"), |_| Color::Red),
        map(tag("green"), |_| Color::Green),
    ))(s)
}

fn color_count(s: &str) -> ParseRes<(u32, Color)> {
    pair(digit, color)(s)
}

#[derive(Debug)]
struct Cubes {
    red: u32,
    blue: u32,
    green: u32,
}

impl Cubes {
    fn new() -> Self {
        Self{
            red: 0,
            blue: 0,
            green: 0,
        }
    }
    fn get_counts(&self) -> Vec<(Color, u32)> {
        vec![
            (Color::Red, self.red),
            (Color::Blue, self.blue),
            (Color::Green, self.green),
        ]
    }
}

fn cubes(s: &str) -> ParseRes<Cubes> {
    map(separated_list0(tag(","), color_count), |color_counts| {
        let mut cubes = Cubes::new();
        for (count, color) in color_counts {
            match color {
                Color::Red => cubes.red = count,
                Color::Green => cubes.green = count,
                Color::Blue => cubes.blue = count,
            }
        }
        cubes
    })(s)
}

struct Game {
    num: u32,
    sets: Vec<Cubes>,
}

fn sets(s: &str) -> ParseRes<Vec<Cubes>> {
    separated_list0(tag(";"), cubes)(s)
}

fn parse_line(line: &str) -> ParseRes<Game> {
    map(tuple((
        tag("Game"),
        digit,
        tag(":"),
        sets,
    )), |(_, num, _, sets)| Game{num, sets}
    )(line)
}
