use std::str::FromStr;
use std::collections::HashSet;

use crate::utils::read_lines;

pub fn p1() {
}

pub fn p2() {
    let mut hands = get_hands("day7.txt");
    hands.sort_by_key(|(hand, _)| hand.total_strength());
    let total: u32 = hands.iter().enumerate().map(|(i, (_, bid))| (i as u32 + 1) * bid).sum();
    println!("{}", total);
}

#[derive(Debug)]
struct Hand((u32, u32, u32, u32, u32));

impl Hand {
    fn as_vec(&self) -> Vec<u32> {
        vec![self.0.0, self.0.1, self.0.2, self.0.3, self.0.4]
    }

    fn strength(&self) -> u32 {
        let counts = get_counts(&self.as_vec());
        if counts == vec![5] {
            6
        } else if counts == vec![1, 4] {
            5
        } else if counts == vec![2, 3] {
            4
        } else if counts == vec![1, 1, 3] {
            3
        } else if counts == vec![1, 2, 2] {
            2
        } else if counts == vec![1, 1, 1, 2] {
            1
        } else if counts == vec![1, 1, 1, 1, 1] {
            0
        } else {
            panic!();
        }
    }

    fn total_strength(&self) -> Vec<u32> {
        let mut values = vec![self.strength()];
        for v in self.as_vec() {
            values.push(v);
        }
        values
    }
}

fn get_counts(cards: &Vec<u32>) -> Vec<u32> {
    let mut values: HashSet<u32> = cards.iter().cloned().collect();
    // remove the jokers
    values.remove(&1);
    let mut counts: Vec<u32> = values.iter().map(|v| count_value(cards, *v)).collect();
    counts.sort();
    // add jokers to the highest count
    let n_counts = counts.len();
    if n_counts == 0 {
        vec![5]
    } else {
        counts[n_counts-1] += count_value(cards, 1);
        counts
    }
}

fn count_value(values: &Vec<u32>, value: u32) -> u32 {
    values.iter().filter(|v| **v == value).count() as u32
}

impl FromStr for Hand {

    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut cards = Vec::new();
        for char in s.chars() {
            cards.push(match char {
                '2'..='9' => {
                    char.to_digit(11).unwrap()
                }
                'A' => 14,
                'K' => 13,
                'Q' => 12,
                'J' => 1,
                'T' => 10,
                _ => panic!(),
            });
        }
        Ok(Hand((cards[0], cards[1], cards[2], cards[3], cards[4])))
    }
}

fn get_hands(file: &str) -> Vec<(Hand, u32)> {
    let mut hands = Vec::new();
    for line in read_lines(file) {
        let (raw_hand, raw_bid) = line.split_once(' ').unwrap();
        hands.push((raw_hand.parse().unwrap(), raw_bid.parse().unwrap()));
    }
    hands
}
