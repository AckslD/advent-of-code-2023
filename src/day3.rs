use std::fmt;
use std::collections::HashMap;

use crate::utils::read_lines;

pub fn p1() {
    let schematic = get_schematic(read_lines("day3.txt"));
    let mut total = 0;
    for (pos, entry) in schematic.entries.iter() {
        match entry {
            Number(n) => {
                if schematic.has_neighbouring_symbol(pos, *n) {
                    total += n;
                }
            }
            _ => {}
        };
    }
    println!("{}", total);
}

pub fn p2() {
    let schematic = get_schematic(read_lines("day3.txt"));
    let mut total: u32 = 0;
    let mut numbers_per_gear: HashMap<Pos, Vec<u32>> = HashMap::new();
    for (pos, entry) in schematic.entries.iter() {
        match entry {
            Number(n) => {
                for p in get_surrounding(pos, n.to_string().len()) {
                    match schematic.entries.get(&p) {
                        Some(Symbol('*')) => {
                            numbers_per_gear.entry(p).or_insert_with(|| Vec::new()).push(*n);
                        }
                        _ => {},
                    }
                }
            }
            _ => {}
        }
    }
    for numbers in numbers_per_gear.values() {
        if numbers.len() == 2 {
            total += numbers.iter().product::<u32>();
        }
    }
    println!("{}", total);
}

#[derive(Debug)]
struct Schematic {
    entries: HashMap<Pos, Entry>
}

impl Schematic {
    fn has_neighbouring_symbol(&self, pos: &Pos, number: u32) -> bool{
        let num_digits = number.to_string().len();
        get_surrounding(pos, num_digits).iter()
            .any(|p| match self.entries.get(p) {
                Some(entry) => {
                    match entry {
                        Symbol(_) => true,
                        Number(_) => false,
                    }
                }
                None => false,
            }
            )
    }
}

fn get_surrounding(pos: &Pos, width: usize) -> Vec<Pos> {
    let mut positions = Vec::new();
    // add left
    if pos.col > 0 {
        if pos.row > 0 {
            positions.push(Pos{row: pos.row - 1, col: pos.col - 1});
        }
        positions.push(Pos{row: pos.row, col: pos.col - 1});
        positions.push(Pos{row: pos.row + 1, col: pos.col - 1});
    }
    // add right
    if pos.row > 0 {
        positions.push(Pos{row: pos.row - 1, col: pos.col + width});
    }
    positions.push(Pos{row: pos.row, col: pos.col + width});
    positions.push(Pos{row: pos.row + 1, col: pos.col + width});
    // add upper
    for col in 0..width {
        // add upper
        if pos.row > 0 {
            positions.push(Pos{row: pos.row - 1, col: pos.col + col});
        }
        // add lower
        positions.push(Pos{row: pos.row + 1, col: pos.col + col});
    }
    positions
}

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
struct Pos {
    row: usize,
    col: usize,
}

impl fmt::Display for Pos {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.row, self.col)
    }
}

#[derive(Debug)]
enum Entry {
    Number(u32),
    Symbol(char),
}

use Entry::*;

fn get_schematic(lines: Vec<String>) -> Schematic {
    let num_cols = lines[0].len();
    let mut entries: HashMap<Pos, Entry> = HashMap::new();
    let mut current_num = String::new();
    for (row, line) in lines.iter().enumerate() {
        for (col, c) in line.chars().enumerate() {
            let pos = Pos{row, col};
            match c {
                '.' => {
                    commit_number(&mut entries, &pos, &mut current_num);
                }
                '0'..='9' => {
                    current_num.push(c);
                }
                _ => {
                    commit_number(&mut entries, &pos, &mut current_num);
                    entries.insert(pos.clone(), Symbol(c));
                }
            }
        }
        commit_number(&mut entries, &Pos{row, col: num_cols}, &mut current_num);
    }
    Schematic{entries}
}

fn commit_number(entries: &mut HashMap<Pos, Entry>, pos: &Pos, current_num: &mut String) {
    if !current_num.is_empty() {
        entries.insert(Pos{row: pos.row, col: pos.col - current_num.len()}, Number(current_num.parse().unwrap()));
        current_num.clear();
    }
}
