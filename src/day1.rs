use std::fs;
use std::collections::HashMap;
use regex::Regex;

pub fn p1() {
    let lines = get_lines("data/day1.txt");
    let total: u32 = lines.iter()
        .map(|line| {
            let digits: Vec<u32> = line.chars().filter_map(|c| c.to_digit(10)).collect();
            10 * digits[0] + digits.last().unwrap()
        })
        .sum();
    println!("{}", total);
}

pub fn p2() {
    let names = vec![
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
    ];
    let mut lookup: HashMap<String, u32> = (0..10).map(|d| (d.to_string(), d)).collect();
    for (d, name) in names.iter().enumerate() {
        lookup.insert(name.to_string(), (d + 1).try_into().unwrap());
    }
    let pattern = lookup.keys().map(|s| s.to_string()).collect::<Vec<String>>().join("|");
    let forward_re = Regex::new(&pattern).unwrap();
    let backward_re = Regex::new(&reverse_string(&pattern)).unwrap();
    let lines = get_lines("data/day1.txt");

    let total: u32 = lines.iter()
        .map(|line| {
            let first = forward_re.find(line).unwrap().as_str();
            let last = reverse_string(backward_re.find(&reverse_string(line)).unwrap().as_str());
            10 * lookup.get(first).unwrap() + lookup.get(&last).unwrap()
        })
        .sum();
    println!("{}", total);
}

fn get_lines(file: &str) -> Vec<String> {
    fs::read_to_string(file)
        .expect("to read file")
        .split('\n')
        .filter(|line| !line.is_empty())
        .map(|line| line.to_string())
        .collect()
}

fn reverse_string(s: &str) -> String {
    s.chars().rev().collect()
}
