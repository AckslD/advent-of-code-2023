use std::collections::HashSet;

use nom::sequence::tuple;
use nom::bytes::complete::tag;
use nom::multi::many1;
use nom::combinator::map;

use crate::utils::read_lines;
use crate::parse::ParseRes;
use crate::parse::digit;

pub fn p1() {
    let lines = read_lines("day4.txt");
    let total: u32 = lines.iter()
        .map(|line| {
            let (_, winning, numbers) = parse_line(&line).unwrap().1;
            let num_wins: u32 = get_num_wins(winning, numbers);
            if num_wins > 0 {
                2u32.pow(num_wins - 1)
            } else {
                0
            }
        })
        .sum();
    println!("{}", total);
}

pub fn p2() {
    let lines = read_lines("day4.txt");
    let mut num_cards: Vec<u32> = (0..lines.len()).map(|_| 1).collect();
    for line in lines {
        let (game, winning, numbers) = parse_line(&line).unwrap().1;
        let num_wins: u32 = get_num_wins(winning, numbers);
        for i in 1..=num_wins {
            num_cards[(game + i - 1) as usize] += num_cards[(game - 1) as usize];
        }
    }
    let total: u32 = num_cards.iter().sum();
    println!("{}", total);
}

fn get_num_wins(winning: Vec<u32>, numbers: Vec<u32>) -> u32 {
    let unique_winning: HashSet<u32> = winning.iter().cloned().collect();
    numbers.iter()
        .filter(|n| unique_winning.contains(n))
        .count().try_into().unwrap()
}

fn numbers(s: &str) -> ParseRes<Vec<u32>> {
    many1(digit)(s)
}

fn parse_line(line: &str) -> ParseRes<(u32, Vec<u32>, Vec<u32>)> {
    map(
        tuple((
            tag("Card"),
            digit,
            tag(":"),
            numbers,
            tag("|"),
            numbers,
        )),
        |(_, num, _, winning, _, numbers)| (num, winning, numbers)
    )(line)
}
