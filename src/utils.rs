use std::fs;

pub fn read_text(file: &str) -> String {
    fs::read_to_string(format!("data/{}", file))
        .expect("to read file")
}

pub fn read_lines(file: &str) -> Vec<String> {
    read_text(file)
        .split('\n')
        .filter(|line| !line.is_empty())
        .map(|line| line.to_string())
        .collect()
}
