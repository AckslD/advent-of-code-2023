use advent2023::day7 as day;

fn main() {
    println!("Problem 1:");
    day::p1();
    println!("\nProblem 2:");
    day::p2();
}
